<?php

use App\ParseStarter;
use App\Parse;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::post('/', 'HomeController@post')->name('index.post');
Route::get('/xlsx', 'HomeController@export');
Route::get('/test', function () {
    $rr = new Parse('https://www.vamsvet.ru/catalog/product/zaglushka_arte_lamp_track_accessories_a210006/');
    dd($rr->parse());
});
