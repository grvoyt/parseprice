<?php

namespace App\Http\Controllers;

use App\Goods;
use App\Jobs\Parsegood;
use App\Parse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $all = Goods::count();
        $count = Goods::where('status', 1)->get()->count();

        return view('welcome', compact('count','all'));
    }

    public function post(Request $request) {
        //Parsegood::dispatch()->onQueue('task');
        return back();
    }

    public function export() {
        //(new UsersExport)->queue('invoices.xlsx','brands');
        return back();
    }
}
