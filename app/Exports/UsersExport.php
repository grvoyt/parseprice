<?php

namespace App\Exports;

use App\Parsed;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;

class UsersExport implements FromQuery
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Parsed::all();
    }

    public function query()
    {
        return Parsed::query();
    }
}
