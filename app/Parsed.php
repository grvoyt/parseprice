<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parsed extends Model
{
    protected $table = 'parsed';
    protected $fillable = ['link_id','title', 'price', 'articul', 'brand', 'description', 'tech', 'imgs'];
    public $timestamps = false;
}
