<?php

namespace App\Jobs;

use App\Goods;
use App\Parse;
use App\Parsed;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class Parsegood implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $elems;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->elems = Goods::all();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $elems = Goods::limit(3)->get();
        $count = 0;
        foreach($elems as $id => $elem) {
            if($count == 20) {
                $count = 0;
                sleep(5);
            }
            try {
                Log::info('start link: '.$elem->id);
                $good = (new Parse($elem->link))->parse();
                Log::info('parsed', $good);
                $parsed = Parsed::create($good);
                $elem->status = 1;
                $elem->save();
                Log::info('url '. $elem->id.' saved');
            }catch(\Exception $e) {
                Log::error('error',[$e->getMessage()]);
            }
            $count++;
        }
    }
}
