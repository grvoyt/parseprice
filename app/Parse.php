<?php
/**
 * Created by PhpStorm.
 * User: grvoyt
 * Date: 14.03.2019
 * Time: 15:15
 */

namespace App;

use GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Sunra\PhpSimple\HtmlDomParser;

// для работы парсера HtmlDomParser
define('MAX_FILE_SIZE', 3000000);

class Parse
{
    const REQUEST_TIMEOUT = 30;
    protected $url;
    protected $site;
    protected $log;
    public $selectors = [
        'title' => '.name-of-main-item',
        'price' => '.change_px_cost',
        'articul' => '.decription-articul',
        'imgs' => '.card-picture .fotorama .hover-fotorama-a',
        'description' => '.block-description-card',
        'tech' => '.technical-block-desc'
    ];
    public function __construct($url)
    {
        $this->site = 'https://www.vamsvet.ru';
        $this->url = $url;
        $this->log = Log::channel('parse');
    }

    public function parse() {
        $result = $this->makeRequest($this->url);
        $dom = HtmlDomParser::str_get_html($result->getBody());
        $elements = $this->findParams($dom);
        $elements['imgs'] = $this->downloadImgs($elements);
        return $elements;
    }

    public function downloadImgs($elements) {
        $imgs = $elements['imgs'];
        $names = [];
        foreach($imgs as $k => $img) {
            $img_content = file_get_contents($this->site.$img);
            $articul_replaced = str_replace(['/',' ','-','.'],'_',$elements['articul']);
            $nameFile = $articul_replaced.'-'.$k.'.jpg';
            $path = $elements['brand'].DIRECTORY_SEPARATOR.$nameFile;
            Storage::disk('brands')->put($path,$img_content);
            $names[] = $nameFile;
        }
        return implode(',',$names);
    }

    public function findParams($dom) {
        $params = [];
        foreach($this->selectors as $title=>$selector) {
            $temp = $this->findElem($dom,$selector);
            switch($title) {
                case 'price':
                    $params[$title] = str_replace(' ','',$temp);
                    break;

                case 'articul':
                    $articulBrand = $this->clearArticul($temp);
                    $params['articul'] = $articulBrand[1];
                    $params['brand'] = $articulBrand[0];
                    break;

                case 'imgs':
                    $params[$title] = $this->findImgs($dom,$selector);
                    break;

                case 'tech':
                    $tech = $this->techSpecs($dom,$selector);
                    $params[$title] = $tech;
                    break;

                default:
                $params[$title] = $temp;
                break;
            }

        }
        return $params;
    }

    protected function techSpecs($dom,$selector) {
        $texts = $dom->find($selector);
        $result = 'Технические характеристики'.PHP_EOL.PHP_EOL;
        foreach($texts as $text) {
            if( !count($text->children()) ) continue;
            $result .= $text->find('.right-technical-name-h3')[0]->innertext.PHP_EOL;
            foreach($text->find('.technical-block-desc-main-one') as $value) {
                $result .= '  '.trim(strip_tags($value)).PHP_EOL;
            }
            $result .= PHP_EOL;
        }
        return $result;
    }

    protected function findImgs($dom,$selector) {
        $imgs = $dom->find($selector);
        $result = [];
        foreach($imgs as $div) {
            $result[] = $div->children[1]->src;
        }
        return $result;
    }

    protected function clearArticul($text) {
        $art = explode('<br>',$text);
        $articul = trim(str_replace('Артикул:','',$art[1]));
        $brand = trim($art[0]);
        return [$brand,$articul];
    }

    protected function findElem($dom,$selector) {
        try{
            $elem = $dom->find($selector);
            if(!count($elem)) return '';
            $res = $elem[0]->innertext;
        } catch (\Exception $e) {

            dd($dom->find($selector));
        }
        return $res;
    }

    protected function makeRequest($url, $async = false, $type = 'get'/* или post */, $form = [], $headers = [], $json = null)
    {
        $method = $type . ($async ? 'Async' : '');

        return (new HttpClient())->$method($url, [
            'timeout' => 5000,
            'verify' => false, // иначе ошибка с ssl-сертификатами
            'allow_redirects' => [
                'track_redirects' => true,
                'max' => 5,
                'protocols' => ['http', 'https'],
            ],
            'form_params' => $form,
            'headers' => $headers,
            'http_errors' => false,
            'json' => $json,
        ]);
    }
}
