<?php
/**
 * Created by PhpStorm.
 * User: grvoyt
 * Date: 15.03.2019
 * Time: 13:05
 */

namespace App;

use Illuminate\Support\Facades\Log;
use App\Goods;
use App\Parse;
use App\Parsed;

class ParseStarter
{
    static public function start($artisan=false) {
        $log = Log::channel('parse');
        Goods::where('status',0)->chunk(100, function($elems) use ($log,$artisan) {
            foreach($elems as $elem) {
                try {
                    $log->info($elem->id.': start link');

                    $good = (new Parse($elem->link))->parse();
                    $good['link_id'] = $elem->id;
                    $log->info('parsed', [$good['title']]);

                    $parsed = Parsed::create($good);

                    $elem->status = 1;
                    $elem->save();
                    if($artisan) $artisan->comment(date('H:i:s').'=> '.$elem->id.' url saved');
                    $log->info('url '. $elem->id.' saved');
                }catch(\Exception $e) {
                    $log->error($elem->id.' error',[$e->getMessage(),$e->getLine(),$e->getFile()]);
                }
            }
            $artisan->comment(date('H:i:s').'=> sleeping');
            sleep(60);
        });
    }
}
