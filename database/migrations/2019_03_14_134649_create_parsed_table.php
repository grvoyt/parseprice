<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParsedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parsed', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title',255);
            $table->integer('price');
            $table->string('articul',255);
            $table->string('brand',255);
            $table->longText('description');
            $table->longText('tech');
            $table->longText('imgs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parsed');
    }
}
